## <a name="31"></a> Question No: 31 DRAG DROP – (Topic 1)
You are developing an application that will include a method named GetData. The GetData() method will retrieve several lines of data from a web service by using a System.IO.StreamReader object.

You have the following requirements:

->The GetData() method must return a string value that contains the entire response from the web service.

->The application must remain responsive while the GetData() method runs.

You need to implement the GetData() method.

How should you complete the relevant code? (To answer, drag the appropriate objects to the correct locations in the answer area. Each object may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_041.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_042.jpg>
</details>



## <a name="32"></a> Question No: 32 – (Topic 1)
You are developing an application that uses the Microsoft ADO.NET Entity Framework to retrieve order information from a Microsoft SQL Server database. The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_043.jpg>

The application must meet the following requirements:

->Return only orders that have an OrderDate value other than null.

->Return only orders that were placed in the year specified in the year parameter.

You need to ensure that the application meets the requirements. Which code segment should you insert at line 08?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_044.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="33"></a> Question No: 33 – (Topic 1)
You are developing an application that will convert data into multiple output formats.

The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_045.jpg>

You are developing a code segment that will produce tab-delimited output. All output routines implement the following interface:

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_046.jpg>

You need to minimize the completion time of the GetOutput() method.

Which code segment should you insert at line 06?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_047.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B

Explanation:

A String object concatenation operation always creates a new object from the existing string and the new data.

A StringBuilder object maintains a buffer to accommodate the concatenation of new data. New data is appended to the buffer if room is available; otherwise, a new, larger buffer is allocated, data from the original buffer is copied to the new buffer, and the new data is then appended to the new buffer.

The performance of a concatenation operation for a String or StringBuilder object depends on the frequency of memory allocations. A String concatenation operation always allocates memory, whereas a StringBuilder concatenation operation allocates memory only if the StringBuilder object buffer is too small to accommodate the new data. Use the String class if you are concatenating a fixed number of String objects. In that case, the compiler may

even combine individual concatenation operations into a single operation. Use a StringBuilder object if you are concatenating an arbitrary number of strings; for example, if you#39;re using a loop to concatenate a random number of strings of user input. http://msdn.microsoft.com/en-us/library/system.text.stringbuilder(v=vs.110).aspx

</details>

## <a name="34"></a> Question No: 34 – (Topic 1)
You are developing an application that includes the following code segment. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_048.jpg>

The GetCustomers() method must meet the following requirements:

->Connect to a Microsoft SQL Server database.

->Populate Customer objects with data from the database.

->Return an IEnumerablelt;Customergt; collection that contains the populated Customer objects.

You need to meet the requirements.

Which two actions should you perform? (Each correct answer presents part of the solution. Choose two.)

Insert the following code segment at line 17:
while (sqlDataReader.GetValues())
```
A Insert the following code segment at line 14: sqlConnection.Open();
B Insert the following code segment at line 14: sqlConnection.BeginTransaction();
C Insert the following code segment at line 17: while (sqlDataReader.Read())
D Insert the following code segment at line 17: while (sqlDataReader.NextResult())
```
<details>
  <summary>Answer</summary>
B,D

 Explanation:

SqlConnection.Open – Opens a database connection with the property settings specified by the

ConnectionString.

http://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlconnection.open.aspx SqlDataReader.Read – Advances the SqlDataReader to the next record. http://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqldatareader.read.aspx

</details>


## <a name="35"></a> Question No: 35 – (Topic 1)
You are developing an application that uses structured exception handling. The application includes a class named Logger. The Logger class implements a method named Log by using the following code segment:

public static void Log(Exception ex) { } You have the following requirements:

->Log all exceptions by using the Log() method of the Logger class.

->Rethrow the original exception, including the entire exception stack.

You need to meet the requirements. Which code segment should you use?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_049.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
D
</details>


## <a name="36"></a> Question No: 36 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until

the process completes.

Which garbage collector method should you use?

```
A ReRegisterForFinalize()
B SuppressFinalize()
C Collect()
D WaitForFullGCApproach()
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="37"></a> Question No: 37 HOTSPOT – (Topic 1)
You have the following code:

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_050.jpg>

For each of the following statements, select Yes if the statement is true. Otherwise, select No.

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_051.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_052.jpg>


Explanation:

Note:

The System.Runtime.Serialization namespace contains classes that can be used for serializing and deserializing objects. Serialization is the process of converting an object or a graph of objects into a linear sequence of bytes for either storage or transmission to another location. Deserialization is the process of taking in stored information and recreating objects from it.
EmitDefaultValue DataMemberAttribute.EmitDefaultValue Property
Gets or sets a value that specifies whether to serialize the default value for a field or property being serialized.

true if the default value for a member should be generated in the serialization stream; otherwise, false.

</details>


## <a name="38"></a> Question No: 38 – (Topic 1)
You are creating an application that manages information about your company#39;s products. The application includes a class named Product and a method named Save.

The Save() method must be strongly typed. It must allow only types inherited from the Product class that use a constructor that accepts no parameters.

You need to implement the Save() method. Which code segment should you use?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_054.jpg>

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="39"></a> Question No: 39 DRAG DROP – (Topic 1)
You have a method named GetCustomerIDs that returns a list of integers. Each entry in the list represents a customer ID that is retrieved from a list named Customers. The Customers list contains 1,000 rows.

Another developer creates a method named ValidateCustomer that accepts an integer parameter and returns a Boolean value. ValidateCustomer returns true if the integer provided references a valid customer. ValidateCustomer can take up to one second to run.

You need to create a method that returns a list of valid customer IDs. The code must execute in the shortest amount of time.

What should you do? (Develop the solution by selecting and ordering the required code snippets. You may not need all of the code snippets.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_055.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_056.jpg>
</details>


Explanation:

<details>
  <summary>Box 1:</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_057.jpg>
</details>

<details>
  <summary>Box 2:</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_058.jpg>
</details>

Note:

* ParallelEnumerable.AsParallel Method Enables parallelization of a query.

/ We parallelize the exution of the ValidateCustomer instances.

## <a name="40"></a> Question No: 40 – (Topic 1)
You are creating a class named Employee. The class exposes a string property named EmployeeType. The following code segment defines the Employee class. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_059.jpg>

The EmployeeType property value must be accessed and modified only by code within the Employee class or within a class derived from the Employee class.

You need to ensure that the implementation of the EmployeeType property meets the requirements.

Which two actions should you perform? (Each correct answer represents part of the complete solution. Choose two.)

```
A Replace line 05 with the following code segment: protected get;
B Replace line 06 with the following code segment: private set;
C Replace line 03 with the following code segment: public string EmployeeType
D Replace line 05 with the following code segment: private get;
E Replace line 03 with the following code segment: protected string EmployeeType
F Replace line 06 with the following code segment: protected set;
```

<details>
  <summary>Answer</summary>
B,E
</details>