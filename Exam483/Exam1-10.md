## <a name="1"></a> Question No: 1 – (Topic 1)
You are creating a class named Game.

The Game class must meet the following requirements:

->Include a member that represents the score for a Game instance.

->Allow external code to assign a value to the score member.

->Restrict the range of values that can be assigned to the score member.

You need to implement the score member to meet the requirements. In which form should you implement the score member?
```
A protected field

B public static field

C public static property

D public property
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="2"></a> Question No: 2 – (Topic 1)
An application includes a class named Person. The Person class includes a method named GetData.

You need to ensure that the GetData() from the Person class. Which access modifier should you use for the GetData() method?
```
A Internal

B Protected

C Private

D Protected internal

E Public
```
<details>
  <summary>Answer</summary>
B

Explanation:

Protected – The type or member can be accessed only by code in the same class or structure, or in a class that is derived from that class.

http://msdn.microsoft.com/en-us/library/ms173121.aspx

The protected keyword is a member access modifier. A protected member is accessible within its class and by derived class instances.

</details>




## <a name="3"></a> Question No: 3 – (Topic 1)
You are developing a C# application that has a requirement to validate some string input data by using the Regex class.

The application includes a method named ContainsHyperlink. The ContainsHyperlink() method will verify the presence of a URI and surrounding markup.

The following code segment defines the ContainsHyperlink() method. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_001.jpg">

The expression patterns used for each validation function are constant.

You need to ensure that the expression syntax is evaluated only once when the Regex object is initially instantiated.

Which code segment should you insert at line 04?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_002.jpg>

```
A Option A

B Option B

C Option C

D Option D
```
<details>
  <summary>Answer</summary>
D

Explanation:

RegexOptions.Compiled – Specifies that the regular expression is compiled to an assembly.This yields faster execution but increases startup time.This value should not be assigned to the Options property when calling the CompileToAssembly method. http://msdn.microsoft.com/en-us/library/system.text.regularexpressions.regexoptions.aspx Additional info

http://stackoverflow.com/questions/513412/how-does-regexoptions-compiled-work

</details>



## <a name="4"></a> Question No: 4 – (Topic 1)
You are developing an application. The application converts a Location object to a string by using a method named WriteObject.

The WriteObject() method accepts two parameters, a Location object and an XmlObjectSerializer object.

The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_003.jpg>

You need to serialize the Location object as XML. Which code segment should you insert at line 20?
```
A new XmlSerializer(typeof(Location))

B new NetDataContractSerializer()

C new DataContractJsonSerializer(typeof (Location))

D new DataContractSerializer(typeof(Location))
```
<details>
  <summary>Answer</summary>
D

Explanation:

The code is using [DataContract] attribute here so need to used DataContractSerializer class.

</details>



## <a name="5"></a> Question No: 5 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.

Which garbage collector method should you use?
```
A WaitForFullGCComplete()

B WaitForFullGCApproach()

C KeepAlive()

D WaitForPendingFinalizers()
```
<details>
  <summary>Answer</summary>
C

Explanation: The GC.KeepAlive method references the specified object, which makes it ineligible for garbage collection from the start of the current routine to the point where this method is called.

The purpose of the KeepAlive method is to ensure the existence of a reference to an object that is at risk of being prematurely reclaimed by the garbage collector.

The KeepAlive method performs no operation and produces no side effects other than

extending the lifetime of the object passed in as a parameter.

</details>


## <a name="6"></a> Question No: 6 – (Topic 1)
You are developing an application that includes a class named BookTracker for tracking library books. The application includes the following code segment. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_004.jpg>

You need to add a book to the BookTracker instance. What should you do?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_005.jpg>

```
A Option A

B Option B

C Option C

D Option D
```

<details>
  <summary>Answer</summary>
A
</details>

## <a name="7"></a> Question No: 7 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server.

You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A HMACSHA256

B RNGCryptoServiceProvider

C DES

D Aes
```

<details>
  <summary>Answer</summary>
A

 Explanation:

The .NET Framework provides the following classes that implement hashing algorithms:

->HMACSHA1.

->MACTripleDES.

->MD5CryptoServiceProvider.

->RIPEMD160.

->SHA1Managed.

->SHA256Managed.

->SHA384Managed.

->SHA512Managed.

HMAC variants of all of the Secure Hash Algorithm (SHA), Message Digest 5 (MD5), and RIPEMD-160 algorithms.

CryptoServiceProvider implementations (managed code wrappers) of all the SHA algorithms.

Cryptography Next Generation (CNG) implementations of all the MD5 and SHA algorithms. http://msdn.microsoft.com/en-us/library/92f9ye3s.aspx#hash_values

</details>



## <a name="8"></a> Question No: 8 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.


Which garbage collector method should you use?
```
A WaitForFullGCComplete()

B SuppressFinalize()

C collect()

D RemoveMemoryPressure()
```

<details>
  <summary>Answer</summary>
B
</details>

## <a name="9"></a> Question No: 9 – (Topic 1)
You are modifying an application that processes leases. The following code defines the Lease class. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_006.jpg>

Leases are restricted to a maximum term of 5 years. The application must send a notification message if a lease request exceeds 5 years.

You need to implement the notification mechanism.

Which two actions should you perform? (Each correct answer presents part of the solution. Choose two.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_007.jpg>

```
A Option A

B Option B

C Option C

D Option D

E Option E

F Option F
```

<details>
  <summary>Answer</summary>
A,B
</details>

## <a name="10"></a> Question No: 10 DRAG DROP – (Topic 1)
You are developing a class named ExtensionMethods.

You need to ensure that the ExtensionMethods class implements the IsEmail() extension method on string objects.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used

once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_008.jpg>

<details>
  <summary>Answer</summary>
  <img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_009.jpg">
</details>
