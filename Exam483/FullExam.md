## EXAM 70-483 C#

<details>
  <summary>Questions</summary>

### [1. Вопрос](#1)

### [2. Вопрос](#2)

### [3. Вопрос](#3)

### [4. Вопрос](#4)

### [5. Вопрос](#5)

### [6. Вопрос](#6)

### [7. Вопрос](#7)

### [8. Вопрос](#8)

### [9. Вопрос](#9)

### [10. Вопрос](#10)

### [11. Вопрос](#11)

### [12. Вопрос](#12)

### [13. Вопрос](#13)

### [14. Вопрос](#14)

### [15. Вопрос](#15)

### [16. Вопрос](#16)

### [17. Вопрос](#17)

### [18. Вопрос](#18)

### [19. Вопрос](#19)

### [20. Вопрос](#20)

### [21. Вопрос](#21)

### [22. Вопрос](#22)

### [23. Вопрос](#23)

### [24. Вопрос](#24)

### [25. Вопрос](#25)

### [26. Вопрос](#26)

### [27. Вопрос](#27)

### [28. Вопрос](#28)

### [29. Вопрос](#29)

### [30. Вопрос](#30)

### [31. Вопрос](#31)

### [32. Вопрос](#32)

### [33. Вопрос](#33)

### [34. Вопрос](#34)

### [35. Вопрос](#35)

### [36. Вопрос](#36)

### [37. Вопрос](#37)

### [38. Вопрос](#38)

### [39. Вопрос](#39)

### [40. Вопрос](#40)

### [41. Вопрос](#41)

### [42. Вопрос](#42)

### [43. Вопрос](#43)

### [44. Вопрос](#44)

### [45. Вопрос](#45)

### [46. Вопрос](#46)

### [47. Вопрос](#47)

### [48. Вопрос](#48)

### [49. Вопрос](#49)

### [50. Вопрос](#50)

### [51. Вопрос](#51)

### [52. Вопрос](#52)

### [53. Вопрос](#53)

### [54. Вопрос](#54)

### [55. Вопрос](#55)

### [56. Вопрос](#56)

### [57. Вопрос](#57)

### [58. Вопрос](#58)

### [59. Вопрос](#59)

### [60. Вопрос](#60)

### [61. Вопрос](#61)

### [62. Вопрос](#62)

### [63. Вопрос](#63)

### [64. Вопрос](#64)

### [65. Вопрос](#65)

### [66. Вопрос](#66)

### [67. Вопрос](#67)

### [68. Вопрос](#68)

### [69. Вопрос](#69)

### [70. Вопрос](#70)

### [71. Вопрос](#71)

### [72. Вопрос](#72)

### [73. Вопрос](#73)

### [74. Вопрос](#74)

### [75. Вопрос](#75)

### [76. Вопрос](#76)

### [77. Вопрос](#77)

### [78. Вопрос](#78)

### [79. Вопрос](#79)

### [80. Вопрос](#80)

### [81. Вопрос](#81)

### [82. Вопрос](#82)

### [83. Вопрос](#83)

### [84. Вопрос](#84)

### [85. Вопрос](#85)

### [86. Вопрос](#86)

### [87. Вопрос](#87)

### [88. Вопрос](#88)

### [89. Вопрос](#89)

### [90. Вопрос](#90)

</details>

---

## <a name="1"></a> Question No: 1 – (Topic 1)
You are creating a class named Game.

The Game class must meet the following requirements:

->Include a member that represents the score for a Game instance.

->Allow external code to assign a value to the score member.

->Restrict the range of values that can be assigned to the score member.

You need to implement the score member to meet the requirements. In which form should you implement the score member?
```
A protected field

B public static field

C public static property

D public property
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="2"></a> Question No: 2 – (Topic 1)
An application includes a class named Person. The Person class includes a method named GetData.

You need to ensure that the GetData() from the Person class. Which access modifier should you use for the GetData() method?
```
A Internal

B Protected

C Private

D Protected internal

E Public
```
<details>
  <summary>Answer</summary>
B

Explanation:

Protected – The type or member can be accessed only by code in the same class or structure, or in a class that is derived from that class.

http://msdn.microsoft.com/en-us/library/ms173121.aspx

The protected keyword is a member access modifier. A protected member is accessible within its class and by derived class instances.

</details>




## <a name="3"></a> Question No: 3 – (Topic 1)
You are developing a C# application that has a requirement to validate some string input data by using the Regex class.

The application includes a method named ContainsHyperlink. The ContainsHyperlink() method will verify the presence of a URI and surrounding markup.

The following code segment defines the ContainsHyperlink() method. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_001.jpg">

The expression patterns used for each validation function are constant.

You need to ensure that the expression syntax is evaluated only once when the Regex object is initially instantiated.

Which code segment should you insert at line 04?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_002.jpg>

```
A Option A

B Option B

C Option C

D Option D
```
<details>
  <summary>Answer</summary>
D

Explanation:

RegexOptions.Compiled – Specifies that the regular expression is compiled to an assembly.This yields faster execution but increases startup time.This value should not be assigned to the Options property when calling the CompileToAssembly method. http://msdn.microsoft.com/en-us/library/system.text.regularexpressions.regexoptions.aspx Additional info

http://stackoverflow.com/questions/513412/how-does-regexoptions-compiled-work

</details>



## <a name="4"></a> Question No: 4 – (Topic 1)
You are developing an application. The application converts a Location object to a string by using a method named WriteObject.

The WriteObject() method accepts two parameters, a Location object and an XmlObjectSerializer object.

The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_003.jpg>

You need to serialize the Location object as XML. Which code segment should you insert at line 20?
```
A new XmlSerializer(typeof(Location))

B new NetDataContractSerializer()

C new DataContractJsonSerializer(typeof (Location))

D new DataContractSerializer(typeof(Location))
```
<details>
  <summary>Answer</summary>
D

Explanation:

The code is using [DataContract] attribute here so need to used DataContractSerializer class.

</details>



## <a name="5"></a> Question No: 5 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.

Which garbage collector method should you use?
```
A WaitForFullGCComplete()

B WaitForFullGCApproach()

C KeepAlive()

D WaitForPendingFinalizers()
```
<details>
  <summary>Answer</summary>
C

Explanation: The GC.KeepAlive method references the specified object, which makes it ineligible for garbage collection from the start of the current routine to the point where this method is called.

The purpose of the KeepAlive method is to ensure the existence of a reference to an object that is at risk of being prematurely reclaimed by the garbage collector.

The KeepAlive method performs no operation and produces no side effects other than

extending the lifetime of the object passed in as a parameter.

</details>


## <a name="6"></a> Question No: 6 – (Topic 1)
You are developing an application that includes a class named BookTracker for tracking library books. The application includes the following code segment. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_004.jpg>

You need to add a book to the BookTracker instance. What should you do?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_005.jpg>

```
A Option A

B Option B

C Option C

D Option D
```

<details>
  <summary>Answer</summary>
A
</details>

## <a name="7"></a> Question No: 7 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server.

You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A HMACSHA256

B RNGCryptoServiceProvider

C DES

D Aes
```

<details>
  <summary>Answer</summary>
A

 Explanation:

The .NET Framework provides the following classes that implement hashing algorithms:

->HMACSHA1.

->MACTripleDES.

->MD5CryptoServiceProvider.

->RIPEMD160.

->SHA1Managed.

->SHA256Managed.

->SHA384Managed.

->SHA512Managed.

HMAC variants of all of the Secure Hash Algorithm (SHA), Message Digest 5 (MD5), and RIPEMD-160 algorithms.

CryptoServiceProvider implementations (managed code wrappers) of all the SHA algorithms.

Cryptography Next Generation (CNG) implementations of all the MD5 and SHA algorithms. http://msdn.microsoft.com/en-us/library/92f9ye3s.aspx#hash_values

</details>



## <a name="8"></a> Question No: 8 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.


Which garbage collector method should you use?
```
A WaitForFullGCComplete()

B SuppressFinalize()

C collect()

D RemoveMemoryPressure()
```

<details>
  <summary>Answer</summary>
B
</details>

## <a name="9"></a> Question No: 9 – (Topic 1)
You are modifying an application that processes leases. The following code defines the Lease class. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_006.jpg>

Leases are restricted to a maximum term of 5 years. The application must send a notification message if a lease request exceeds 5 years.

You need to implement the notification mechanism.

Which two actions should you perform? (Each correct answer presents part of the solution. Choose two.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_007.jpg>

```
A Option A

B Option B

C Option C

D Option D

E Option E

F Option F
```

<details>
  <summary>Answer</summary>
A,B
</details>

## <a name="10"></a> Question No: 10 DRAG DROP – (Topic 1)
You are developing a class named ExtensionMethods.

You need to ensure that the ExtensionMethods class implements the IsEmail() extension method on string objects.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used

once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_008.jpg>

<details>
  <summary>Answer</summary>
  <img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_009.jpg">
</details>

## <a name="11"></a> Question No: 11 DRAG DROP – (Topic 1)
You develop an application that displays information from log files when errors occur. The application will prompt the user to create an error report that sends details about the error and the session to the administrator.

When a user opens a log file by using the application, the application throws an exception and closes.

The application must preserve the original stack trace information when an exception occurs during this process.

You need to implement the method that reads the log files.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_010.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_011.jpg">

Explanation:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_012.jpg">

StringReader – Implements a TextReader that reads from a string. http://msdn.microsoft.com/en-us/library/system.io.stringreader(v=vs.110).aspx StreamReader – Implements a TextReader that reads characters from a byte stream in a particular encoding.

http://msdn.microsoft.com/en-us/library/system.io.streamreader(v=vs.110).aspx

Once an exception is thrown, part of the information it carries is the stack trace. The stack trace is a list of the method call hierarchy that starts with the method that throws the exception and ends with the method that catches the exception. If an exception is re- thrown by specifying the exception in the throw statement, the stack trace is restarted at the current method and the list of method calls between the original method that threw the exception and the current method is lost. To keep the original stack trace information with the exception, use the throw statement without specifying the exception. http://msdn.microsoft.com/en-us/library/ms182363(v=vs.110).aspx

</details>





## <a name="12"></a> Question No: 12 – (Topic 1)
You have a List object that is generated by executing the following code:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_013.jpg">

You have a method that contains the following code (line numbers are included for reference only):

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_014.jpg">

You need to alter the method to use a lambda statement.

How should you rewrite lines 03 through 06 of the method?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_015.jpg">

```
A Option A

B Option B

C Option C

D Option D
```
<details>
  <summary>Answer</summary>
C
</details>

## <a name="13"></a> Question No: 13 – (Topic 1)
You are developing an assembly that will be used by multiple applications. You need to install the assembly in the Global Assembly Cache (GAC).

Which two actions can you perform to achieve this goal? (Each correct answer presents a complete solution. Choose two.)

Use the Assembly Registration tool (regasm.exe) to register the assembly and to copy the assembly to the GAC.
```
A Use the Strong Name tool (sn.exe) to copy the assembly into the GAC.

B Use Microsoft Register Server (regsvr32.exe) to add the assembly to the GAC.

C Use the Global Assembly Cache tool (gacutil.exe) to add the assembly to the GAC.

D Use Windows Installer 2.0 to add the assembly to the GAC.
```

<details>
  <summary>Answer</summary>
D,E

Explanation:

There are two ways to deploy an assembly into the global assembly cache:

Use an installer designed to work with the global assembly cache. This is the preferred option for installing assemblies into the global assembly cache.

Use a developer tool called the Global Assembly Cache tool (Gacutil.exe), provided by the Windows

Software Development Kit (SDK). Note:

In deployment scenarios, use Windows Installer 2.0 to install assemblies into the global assembly cache. Use the Global Assembly Cache tool only in development scenarios, because it does not provide assembly reference counting and other features provided when using the Windows Installer.

http://msdn.microsoft.com/en-us/library/yf1d93sz(v=vs.110).aspx

</details>



## <a name="14"></a> Question No: 14 DRAG DROP – (Topic 1)
You are developing an application that will populate an extensive XML tree from a Microsoft SQL Server 2008 R2 database table named Contacts.

You are creating the XML tree. The solution must meet the following requirements:

->Minimize memory requirements.

->Maximize data processing speed.

You open the database connection. You need to create the XML tree.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_016.jpg">

Answer:

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_017.jpg">
</details>


## <a name="15"></a> Question No: 15 – (Topic 1)
You are debugging an application that calculates loan interest. The application includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_018.jpg">

You have the following requirements:

->The debugger must break execution within the Calculatelnterest() method when the loanAmount variable is less than or equal to zero.

->The release version of the code must not be impacted by any changes.

You need to meet the requirements. What should you do?
```
A Insert the following code segment at tine 05: Debug.Write(loanAmount gt; 0);
B Insert the following code segment at line 05: Trace.Write(loanAmount gt; 0);
C Insert the following code segment at line 03: Debug.Assert(loanAmount gt; 0);
D Insert the following code segment at line 03: Trace.Assert(loanAmount gt; 0);
```
<details>
  <summary>Answer</summary>
C

Explanation:

By default, the Debug.Assert method works only in debug builds. Use the Trace.Assert method if you want to do assertions in release builds. For more information, see Assertions in Managed Code.

http://msdn.microsoft.com/en-us/library/kssw4w7z.aspx

</details> 



## <a name="16"></a> Question No: 16 DRAG DROP – (Topic 1)
You are developing an application that includes a class named Kiosk. The Kiosk class includes a static property named Catalog. The Kiosk class is defined by the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_019.jpg">

You have the following requirements:

->Initialize the _catalog field to a Catalog instance.

->Initialize the _catalog field only once.

->Ensure that the application code acquires a lock only when the _catalog object must be instantiated.

You need to meet the requirements.

Which three code segments should you insert in sequence at line 09? (To answer, move the appropriate code segments from the list of code segments to the answer area and arrange them in the correct order.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_020.jpg">

Answer:

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_021.jpg">

Explanation:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_022.jpg">

D:\Documents and Settings\useralbo\Desktop\1.jpg

After taking a lock you must check once again the _catalog field to be sure that other threads didn#39;t instantiated it in the meantime.
</details>




## <a name="17"></a> Question No: 17 – (Topic 1)
You are developing an application. The application calls a method that returns an array of integers named customerIds. You define an integer variable named customerIdToRemove and assign a value to it. You declare an array named filteredCustomerIds.

You have the following requirements.

->Remove duplicate integers from the customerIds array.

->Sort the array in order from the highest value to the lowest value.

->Remove the integer value stored in the customerIdToRemove variable from the customerIds array.

You need to create a LINQ query to meet the requirements. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_023.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
C
</details>

## <a name="18"></a> Question No: 18 – (Topic 1)
You are developing an application that includes a class named Order. The application will store a collection of Order objects.

The collection must meet the following requirements:

->Internally store a key and a value for each collection item.

->Provide objects to iterators in ascending order based on the key.

->Ensure that item are accessible by zero-based index or by key.

You need to use a collection type that meets the requirements. Which collection type should you use?
```
A LinkedList
B Queue
C Array
D HashTable
E SortedList
```
<details>
  <summary>Answer</summary>
E

SortedListlt;TKey, TValuegt; – Represents a collection of key/value pairs that are sorted by key based on the associated IComparerlt;Tgt; implementation. http://msdn.microsoft.com/en-us/library/ms132319.aspx

</details>


## <a name="19"></a> Question No: 19 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.

Which garbage collector method should you use?
```
A WaitForFullGCComplete()
B SuppressFinalize()
C WaitForFullGCApproach()
D WaitForPendingFinalizers()
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="20"></a> Question No: 20 – (Topic 1)
You are developing an application that includes a class named BookTracker for tracking library books. The application includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_024.jpg">

You need to add a user to the BookTracker instance. What should you do?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_025.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="21"></a> Question No: 21 – (Topic 1)
You are creating an application that manages information about zoo animals. The application includes a class named Animal and a method named Save.

The Save() method must be strongly typed. It must allow only types inherited from the Animal class that uses a constructor that accepts no parameters.

You need to implement the Save() method. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_026.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
C

 Explanation:

When you define a generic class, you can apply restrictions to the kinds of types that client code can use for type arguments when it instantiates your class. If client code tries to instantiate your class by using a type that is not allowed by a constraint, the result is a compile-time error. These restrictions are called constraints. Constraints are specified by using the where contextual keyword.

http://msdn.microsoft.com/en-us/library/d5x73970.aspx
</details>



## <a name="22"></a> Question No: 22 – (Topic 1)
You are creating an application that manages information about your company#39;s products. The application includes a class named Product and a method named Save.

The Save() method must be strongly typed. It must allow only types inherited from the Product class that use a constructor that accepts no parameters.

You need to implement the Save() method. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_027.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
D

Explanation:

When you define a generic class, you can apply restrictions to the kinds of types that client code can use for type arguments when it instantiates your class. If client code tries to instantiate your class by using a type that is not allowed by a constraint, the result is a compile-time error. These restrictions are called constraints.

Constraints are specified by using the where contextual keyword. http://msdn.microsoft.com/en-us/library/d5x73970.aspx
</details>



## <a name="23"></a> Question No: 23 – (Topic 1)
An application receives JSON data in the following format:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_028.jpg">

The application includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_029.jpg">

You need to ensure that the ConvertToName() method returns the JSON input string as a Name object.

Which code segment should you insert at line 10?
```
A Return ser.Desenalize (json, typeof(Name));
B Return ser.ConvertToType<Name>(json);
C Return ser.Deserialize<Name>(json);
D Return ser.ConvertToType (json, typeof (Name));
```
<details>
  <summary>Answer</summary>
C
</details>

## <a name="24"></a> Question No: 24 – (Topic 1)
You are developing an application that uses several objects. The application includes the

following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_030.jpg">

You need to evaluate whether an object is null. Which code segment should you insert at line 03?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_031.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
D

Explanation: Use the == operator to compare values and in this case also use the null literal.
</details>


## <a name="25"></a> Question No: 25 DRAG DROP – (Topic 1)
You are developing an application by using C#. The application includes an array of decimal values named loanAmounts. You are developing a LINQ query to return the values from the array.

The query must return decimal values that are evenly divisible by two. The values must be sorted from the lowest value to the highest value.

You need to ensure that the query correctly returns the decimal values.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_032.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_033.jpg">

Explanation:

Box 1: from

Box 2: where

Box 3: orderby Box 4: ascending

Box 5: select

Note: In a query expression, the orderby clause causes the returned sequence or subsequence (group) to be sorted in either ascending or descending order.

Examples:

// Query for ascending sort. IEnumerablelt;stringgt; sortAscendingQuery = from fruit in fruits

orderby fruit //quot;ascendingquot; is default select fruit;

// Query for descending sort. IEnumerablelt;stringgt; sortDescendingQuery = from w in fruits

orderby w descending select w;
</details>





## <a name="26"></a> Question No: 26 DRAG DROP – (Topic 1)
You are developing a custom collection named LoanCollection for a class named Loan class.

You need to ensure that you can process each Loan object in the LoanCollection collection

by using a foreach loop.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_034.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_035.jpg">
</details>



## <a name="27"></a> Question No: 27 HOTSPOT – (Topic 1)
You are developing an application in C#.

The application will display the temperature and the time at which the temperature was recorded. You have the following method (line numbers are included for reference only):

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_036.jpg">

You need to ensure that the message displayed in the lblMessage object shows the time formatted according to the following requirements:

->The time must be formatted as hour:minute AM/PM, for example 2:00 PM.

->The date must be formatted as month/day/year, for example 04/21/2013.

->The temperature must be formatted to have two decimal places, for example 23- 45.

Which code should you insert at line 04? (To answer, select the appropriate options in the answer area.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_037.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_038.jpg">
</details>



## <a name="28"></a> Question No: 28 – (Topic 1)
You are developing an application. The application includes a method named ReadFile that reads data from a file.

The ReadFile() method must meet the following requirements:

->It must not make changes to the data file.

->It must allow other processes to access the data file.

->It must not throw an exception if the application attempts to open a data file that does not exist.

You need to implement the ReadFile() method. Which code segment should you use?
```
A var fs = File.Open(Filename, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
B var fs = File.Open(Filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
C var fs = File.Open(Filename, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Write);
D var fs = File.ReadAllLines(Filename);
E var fs = File.ReadAllBytes(Filename);
```
<details>
  <summary>Answer</summary>
A

Explanation:

FileMode.OpenOrCreate – Specifies that the operating system should open a file if it exists; otherwise, a new file should be created. If the file is opened with FileAccess.Read, FileIOPermissionAccess.Read permission is required. If the file access is FileAccess.Write, FileIOPermissionAccess.Write permission is required. If the file is opened with FileAccess.ReadWrite, both FileIOPermissionAccess.Read and FileIOPermissionAccess.Write permissions are required.

http://msdn.microsoft.com/en-us/library/system.io.filemode.aspx

FileShare.ReadWrite – Allows subsequent opening of the file for reading or writing.If this flag is not specified, any request to open the file for reading or writing (by this process or another process) will fail until the file is closed.However, even if this flag is specified, additional permissions might still be needed to access the file. http://msdn.microsoft.com/pl-pl/library/system.io.fileshare.aspx

</details>


## <a name="29"></a> Question No: 29 – (Topic 1)
You are implementing a method named FloorTemperature that performs conversions between value types and reference types. The following code segment implements the method. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_039.jpg">

You need to ensure that the application does not throw exceptions on invalid conversions. Which code segment should you insert at line 04?
```
A int result = (int)degreesRef;
B int result = (int)(double)degreesRef;
C int result = degreesRef;
D int result = (int)(float)degreesRef;
```
<details>
  <summary>Answer</summary>
D
</details>

## <a name="30"></a> Question No: 30 – (Topic 1)
You are developing an application that includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_040.jpg">

You need to ensure that the application accepts only integer input and prompts the user each time non-integer input is entered.

Which code segment should you add at line 19?

```
A If (!int.TryParse(sLine, out number))
B If ((number = Int32.Parse(sLine)) == Single.NaN)
C If ((number = int.Parse(sLine)) > Int32.MaxValue)
D If (Int32.TryParse(sLine, out number))
```
<details>
  <summary>Answer</summary>
A

Explanation:

B and C will throw exception when user enters non-integer value. D is exactly the opposite what we want to achieve.
</details>

## <a name="31"></a> Question No: 31 DRAG DROP – (Topic 1)
You are developing an application that will include a method named GetData. The GetData() method will retrieve several lines of data from a web service by using a System.IO.StreamReader object.

You have the following requirements:

->The GetData() method must return a string value that contains the entire response from the web service.

->The application must remain responsive while the GetData() method runs.

You need to implement the GetData() method.

How should you complete the relevant code? (To answer, drag the appropriate objects to the correct locations in the answer area. Each object may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_041.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_042.jpg>
</details>



## <a name="32"></a> Question No: 32 – (Topic 1)
You are developing an application that uses the Microsoft ADO.NET Entity Framework to retrieve order information from a Microsoft SQL Server database. The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_043.jpg>

The application must meet the following requirements:

->Return only orders that have an OrderDate value other than null.

->Return only orders that were placed in the year specified in the year parameter.

You need to ensure that the application meets the requirements. Which code segment should you insert at line 08?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_044.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="33"></a> Question No: 33 – (Topic 1)
You are developing an application that will convert data into multiple output formats.

The application includes the following code. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_045.jpg>

You are developing a code segment that will produce tab-delimited output. All output routines implement the following interface:

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_046.jpg>

You need to minimize the completion time of the GetOutput() method.

Which code segment should you insert at line 06?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_047.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B

Explanation:

A String object concatenation operation always creates a new object from the existing string and the new data.

A StringBuilder object maintains a buffer to accommodate the concatenation of new data. New data is appended to the buffer if room is available; otherwise, a new, larger buffer is allocated, data from the original buffer is copied to the new buffer, and the new data is then appended to the new buffer.

The performance of a concatenation operation for a String or StringBuilder object depends on the frequency of memory allocations. A String concatenation operation always allocates memory, whereas a StringBuilder concatenation operation allocates memory only if the StringBuilder object buffer is too small to accommodate the new data. Use the String class if you are concatenating a fixed number of String objects. In that case, the compiler may

even combine individual concatenation operations into a single operation. Use a StringBuilder object if you are concatenating an arbitrary number of strings; for example, if you#39;re using a loop to concatenate a random number of strings of user input. http://msdn.microsoft.com/en-us/library/system.text.stringbuilder(v=vs.110).aspx

</details>

## <a name="34"></a> Question No: 34 – (Topic 1)
You are developing an application that includes the following code segment. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_048.jpg>

The GetCustomers() method must meet the following requirements:

->Connect to a Microsoft SQL Server database.

->Populate Customer objects with data from the database.

->Return an IEnumerablelt;Customergt; collection that contains the populated Customer objects.

You need to meet the requirements.

Which two actions should you perform? (Each correct answer presents part of the solution. Choose two.)

Insert the following code segment at line 17:
while (sqlDataReader.GetValues())
```
A Insert the following code segment at line 14: sqlConnection.Open();
B Insert the following code segment at line 14: sqlConnection.BeginTransaction();
C Insert the following code segment at line 17: while (sqlDataReader.Read())
D Insert the following code segment at line 17: while (sqlDataReader.NextResult())
```
<details>
  <summary>Answer</summary>
B,D

 Explanation:

SqlConnection.Open – Opens a database connection with the property settings specified by the

ConnectionString.

http://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlconnection.open.aspx SqlDataReader.Read – Advances the SqlDataReader to the next record. http://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqldatareader.read.aspx

</details>


## <a name="35"></a> Question No: 35 – (Topic 1)
You are developing an application that uses structured exception handling. The application includes a class named Logger. The Logger class implements a method named Log by using the following code segment:

public static void Log(Exception ex) { } You have the following requirements:

->Log all exceptions by using the Log() method of the Logger class.

->Rethrow the original exception, including the entire exception stack.

You need to meet the requirements. Which code segment should you use?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_049.jpg>

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
D
</details>


## <a name="36"></a> Question No: 36 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until

the process completes.

Which garbage collector method should you use?

```
A ReRegisterForFinalize()
B SuppressFinalize()
C Collect()
D WaitForFullGCApproach()
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="37"></a> Question No: 37 HOTSPOT – (Topic 1)
You have the following code:

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_050.jpg>

For each of the following statements, select Yes if the statement is true. Otherwise, select No.

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_051.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_052.jpg>


Explanation:

Note:

The System.Runtime.Serialization namespace contains classes that can be used for serializing and deserializing objects. Serialization is the process of converting an object or a graph of objects into a linear sequence of bytes for either storage or transmission to another location. Deserialization is the process of taking in stored information and recreating objects from it.
EmitDefaultValue DataMemberAttribute.EmitDefaultValue Property
Gets or sets a value that specifies whether to serialize the default value for a field or property being serialized.

true if the default value for a member should be generated in the serialization stream; otherwise, false.

</details>


## <a name="38"></a> Question No: 38 – (Topic 1)
You are creating an application that manages information about your company#39;s products. The application includes a class named Product and a method named Save.

The Save() method must be strongly typed. It must allow only types inherited from the Product class that use a constructor that accepts no parameters.

You need to implement the Save() method. Which code segment should you use?

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_054.jpg>

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="39"></a> Question No: 39 DRAG DROP – (Topic 1)
You have a method named GetCustomerIDs that returns a list of integers. Each entry in the list represents a customer ID that is retrieved from a list named Customers. The Customers list contains 1,000 rows.

Another developer creates a method named ValidateCustomer that accepts an integer parameter and returns a Boolean value. ValidateCustomer returns true if the integer provided references a valid customer. ValidateCustomer can take up to one second to run.

You need to create a method that returns a list of valid customer IDs. The code must execute in the shortest amount of time.

What should you do? (Develop the solution by selecting and ordering the required code snippets. You may not need all of the code snippets.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_055.jpg>

<details>
  <summary>Answer</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_056.jpg>
</details>


Explanation:

<details>
  <summary>Box 1:</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_057.jpg>
</details>

<details>
  <summary>Box 2:</summary>
<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_058.jpg>
</details>

Note:

* ParallelEnumerable.AsParallel Method Enables parallelization of a query.

/ We parallelize the exution of the ValidateCustomer instances.

## <a name="40"></a> Question No: 40 – (Topic 1)
You are creating a class named Employee. The class exposes a string property named EmployeeType. The following code segment defines the Employee class. (Line numbers are included for reference only.)

<img src=http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_059.jpg>

The EmployeeType property value must be accessed and modified only by code within the Employee class or within a class derived from the Employee class.

You need to ensure that the implementation of the EmployeeType property meets the requirements.

Which two actions should you perform? (Each correct answer represents part of the complete solution. Choose two.)

```
A Replace line 05 with the following code segment: protected get;
B Replace line 06 with the following code segment: private set;
C Replace line 03 with the following code segment: public string EmployeeType
D Replace line 05 with the following code segment: private get;
E Replace line 03 with the following code segment: protected string EmployeeType
F Replace line 06 with the following code segment: protected set;
```

<details>
  <summary>Answer</summary>
B,E
</details>

## <a name="41"></a> Question No: 41 – (Topic 1)
You are developing a method named CreateCounters that will create performance counters for an application. The method includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_060.jpg">

You need to ensure that Counter2 is available for use in Windows Performance Monitor (PerfMon).

Which code segment should you insert at line 16?
```
A CounterType = PerformanceCounterType.RawBase
B CounterType = PerformanceCounterType.AverageBase
C CounterType = PerformanceCounterType.SampleBase
D CounterType = PerformanceCounterType.CounterMultiBase
```
<details>
  <summary>Answer</summary>
D

Explanation:

PerformanceCounterType.AverageTimer32 – An average counter that measures the time it takes, on average, to complete a process or operation. Counters of this type display a ratio of the total elapsed time of the sample interval to the number of processes or operations completed during that time. This counter type measures time in ticks of the system clock.

Formula: ((N 1 -N 0)/F)/(B 1 -B 0), where N 1 and N 0 are performance counter readings, B 1 and B 0 are their corresponding AverageBase values, and F is the number of ticks per second. The value of F is factored into the equation so that the result can be displayed in seconds.

Thus, the numerator represents the numbers of ticks counted during the last sample interval, F represents the frequency of the ticks, and the denominator represents the number of operations completed during the last sample interval. Counters of this type include PhysicalDisk\ Avg. Disk sec/Transfer.

PerformanceCounterType.AverageBase – A base counter that is used in the calculation of time or count averages, such as AverageTimer32 and AverageCount64. Stores the denominator for calculating a counter to present quot;time per operationquot; or quot;count per operationquot;..

http://msdn.microsoft.com/en-us/library/system.diagnostics.performancecountertype.aspx

</details> 


## <a name="42"></a> Question No: 42 – (Topic 1)
You are developing an application that uses the Microsoft ADO.NET Entity Framework to retrieve order information from a Microsoft SQL Server database. The application includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_061.jpg">

The application must meet the following requirements:

->Return only orders that have an OrderDate value other than null.

->Return only orders that were placed in the year specified in the OrderDate property or in a later year.

You need to ensure that the application meets the requirements. Which code segment should you insert at line 08?
```
A Where order.OrderDate.Value != null && order.OrderDate.Value.Year >= year
B Where order.OrderDate.Value == null && order.OrderDate.Value.Year == year
C Where order.OrderDate.HasValue && order.OrderDate.Value.Year == year
D Where order.OrderDate.Value.Year == year
```
<details>
  <summary>Answer</summary>
A

Explanation: *For the requirement to use an OrderDate value other than null use: OrderDate.Value != null

*For the requirement to use an OrderDate value for this year or a later year use: OrderDate.Valuegt;= year

</details>



## <a name="43"></a> Question No: 43 DRAG DROP – (Topic 1)
You have the following class:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_062.jpg">

You need to implement IEquatable. The Equals method must return true if both ID and Name are set to the identical values. Otherwise, the method must return false. Equals must not throw an exception.

What should you do? (Develop the solution by selecting and ordering the required code snippets. You may not need all of the code snippets.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_063.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_064.jpg">
</details>



## <a name="44"></a> Question No: 44 – (Topic 1)
You are developing an application. The application includes classes named Employee and Person and an interface named IPerson.

The Employee class must meet the following requirements:

->It must either inherit from the Person class or implement the IPerson interface.

->It must be inheritable by other classes in the application.

You need to ensure that the Employee class meets the requirements.

Which two code segments can you use to achieve this goal? (Each correct answer presents a complete solution. Choose two.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_065.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B,D


Explanation:

Sealed – When applied to a class, the sealed modifier prevents other classes from inheriting from it. http://msdn.microsoft.com/en-us/library/88c54tsw(v=vs.110).aspx

</details> 

## <a name="45"></a> Question No: 45 HOTSPOT – (Topic 1)
You are reviewing the following code:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_066.jpg">

For each of the following statements, select Yes if the statement is true. Otherwise, select No.

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_067.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_068.jpg">
</details>

## <a name="46"></a> Question No: 46 – (Topic 1)
An application includes a class named Person. The Person class includes a method named GetData.

You need to ensure that the GetData() method can be used only by the Person class and not by any class derived from the Person class.

Which access modifier should you use for the GetData() method?

```
A Public
B Protected internal
C Internal
D Private
E Protected
```
<details>
  <summary>Answer</summary>
D

The GetData() method should be private. It would then only be visible within the Person class.

</details> 


## <a name="47"></a> Question No: 47 – (Topic 1)
You are developing an application by using C#. You provide a public key to the development team during development.

You need to specify that the assembly is not fully signed when it is built.

Which two assembly attributes should you include in the source code? (Each correct answer presents part of the solution. Choose two.)

```
A AssemblyFlagsAttribute
B AssemblyKeyFileAttribute
C AssemblyConfigurationAttribute
D AssemblyDelaySignAttribute
```
<details>
  <summary>Answer</summary>
B,D
</details>

## <a name="48"></a> Question No: 48 DRAG DROP – (Topic 1)
You are testing an application. The application includes methods named CalculateInterest and LogLine. The CalculateInterest() method calculates loan interest. The LogLine() method sends diagnostic messages to a console window.

You have the following requirements:

->The CalculateInterest() method must run for all build configurations.

->The LogLine() method must be called only for debug builds.

You need to ensure that the methods run correctly.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_070.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_071.jpg">
</details>



## <a name="49"></a> Question No: 49 DRAG DROP – (Topic 1)
An application serializes and deserializes XML from streams. The XML streams are in the following format:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_072.jpg">

The application reads the XML streams by using a DataContractSerializer object that is declared by the following code segment:

var ser = new DataContractSerializer(typeof(Name));

You need to ensure that the application preserves the element ordering as provided in the XML stream.

How should you complete the relevant code? (To answer, drag the appropriate attributes to the correct locations in the answer area-Each attribute may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_073.jpg">

<details>
  <summary>Answer</summary>

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_074.jpg">

Explanation:



DataContractAttribute – Specifies that the type defines or implements a data contract and is

serializable by a serializer, such as the DataContractSerializer. To make their type serializable, type authors must define a data contract for their type.

http://msdn.microsoft.com/en- us/library/system.runtime.serialization.datacontractattribute.aspx

DataMemberAttribute – When applied to the member of a type, specifies that the member is part of a data contract and is serializable by the DataContractSerializer.

http://msdn.microsoft.com/en-us/library/ms574795.aspx

</details>


## <a name="50"></a> Question No: 50 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server. You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A ECDsa
B RNGCryptoServiceProvider
C Rfc2898DeriveBytes
D HMACSHA512
```
<details>
  <summary>Answer</summary>
D
</details>

## <a name="51"></a> Question No: 51 – (Topic 1)
You are developing an application. The application converts a Location object to a string by using a method named WriteObject. The WriteObject() method accepts two parameters, a Location object and an XmlObjectSerializer object.

The application includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_076.jpg">

You need to serialize the Location object as a JSON object. Which code segment should you insert at line 20?

```
A New DataContractSerializer(typeof(Location))
B New XmlSerializer(typeof(Location))
C New NetDataContractSenalizer()
D New DataContractJsonSerializer(typeof(Location))
```
<details>
  <summary>Answer</summary>
D

Explanation:

The DataContractJsonSerializer class serializes objects to the JavaScript Object Notation (JSON) and deserializes JSON data to objects.

Use the DataContractJsonSerializer class to serialize instances of a type into a JSON document and to deserialize a JSON document into an instance of a type.

</details> 

## <a name="52"></a> Question No: 52 – (Topic 1)
You are developing an application. The application includes classes named Mammal and Animal and an interface named IAnimal.

The Mammal class must meet the following requirements:

->It must either inherit from the Animal class or implement the IAnimal interface.

->It must be inheritable by other classes in the application.

You need to ensure that the Mammal class meets the requirements.

Which two code segments can you use to achieve this goal? (Each correct answer presents a complete solution. Choose two.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_077.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
   A,C 

   Explanation:

When applied to a class, the sealed modifier prevents other classes from inheriting from it. http://msdn.microsoft.com/en-us/library/88c54tsw(v=vs.110).aspx
</details> 


## <a name="53"></a> Question No: 53 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server. You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A DES
B HMACSHA512
C RNGCryptoServiceProvider
D ECDsa
```
<details>
  <summary>Answer</summary>
   B
</details> 


## <a name="54"></a> Question No: 54 – (Topic 1)
You are developing a method named CreateCounters that will create performance counters for an application.

The method includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_078.jpg">

You need to ensure that Counter1 is available for use in Windows Performance Monitor (PerfMon).

Which code segment should you insert at line 16?

```
A CounterType = PerformanccCounterType.RawBase
B CounterType = PerformanceCounterType.AverageBase
C CounterType = PerformanceCounterType.SampleBase
D CounterType = PerformanceCounterType.CounterMultiBase
```

<details>
  <summary>Answer</summary>
C 

Explanation:

PerformanceCounterType.SampleBase – A base counter that stores the number of sampling interrupts taken and is used as a denominator in the sampling fraction. The sampling fraction is the number of samples that were 1 (or true) for a sample interrupt. Check that this value is greater than zero before using it as the denominator in a calculation of SampleFraction.

PerformanceCounterType.SampleFraction – A percentage counter that shows the average ratio of hits to all operations during the last two sample intervals. Formula: ((N 1 – N 0) / (D 1 – D 0)) x 100, where the numerator represents the number of successful operations during the last sample interval, and the denominator represents the change in the number of all operations (of the type measured) completed during the sample interval, using counters of type SampleBase. Counters of this type include Cache\Pin Read Hits %. http://msdn.microsoft.com/en-us/library/system.diagnostics.performancecountertype.aspx

</details> 


## <a name="55"></a> Question No: 55 – (Topic 1)
You are creating a console application named App1.

App1 retrieves data from the Internet by using JavaScript Object Notation (JSON).

You are developing the following code segment (line numbers are included for reference only):

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_079.jpg">

You need to ensure that the code validates the JSON string. Which code should you insert at line 03?

```
A DataContractSerializer serializer = new DataContractSerializer();
B var serializer = new DataContractSerializer();
C XmlSerlalizer serializer = new XmlSerlalizer();
D var serializer = new JavaScriptSerializer();
```

<details>
  <summary>Answer</summary>
   D

   Explanation: The JavaScriptSerializer Class Provides serialization and deserialization functionality for AJAX-enabled applications.

The JavaScriptSerializer class is used internally by the asynchronous communication layer to serialize and deserialize the data that is passed between the browser and the Web server. You cannot access that instance of the serializer. However, this class exposes a public API. Therefore, you can use the class when you want to work with JavaScript Object Notation (JSON) in managed code.

</details>


## <a name="56"></a> Question No: 56 DRAG DROP – (Topic 1)
You are developing an application that implements a set of custom exception types. You declare the custom exception types by using the following code segments:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_080.jpg">

The application includes a function named DoWork that throws .NET Framework exceptions and custom exceptions.

The application contains only the following logging methods:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_081.jpg">

The application must meet the following requirements:

->When AdventureWorksValidationException exceptions are caught, log the information by using the static void Log (AdventureWorksValidationException ex) method.

->When AdventureWorksDbException or other AdventureWorksException

exceptions are caught, log the information by using the static void I oq( AdventureWorksException ex) method.

You need to meet the requirements.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_082.jpg">

<details>
  <summary>Answer</summary>

   <img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_083.jpg">

</details> 



## <a name="57"></a> Question No: 57 – (Topic 1)
You are developing an application that will process orders. The debug and release versions of the application will display different logo images.

You need to ensure that the correct image path is set based on the build configuration. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_084.jpg">

```
A Option A
B Option B
C Option C
D Option D
``` 

<details>
  <summary>Answer</summary>
   C

   Explanation:

There is no such constraint (unless you define one explicitly) RELEASE. http://stackoverflow.com/questions/507704/will-if-release-work-like-if-debug-does-in-c
</details> 


## <a name="58"></a> Question No: 58 DRAG DROP – (Topic 1)
You are developing an application by using C#. The application will process several objects per second.

You need to create a performance counter to analyze the object processing.

Which three actions should you perform in sequence? (To answer, move the appropriate actions from the list of actions to the answer area and arrange them in the correct order.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_085.jpg">

<details>
  <summary>Answer</summary>

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_086.jpg">

Explanation:

Box 1:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_087.jpg">

Box 2:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_088.jpg">

Box 3:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_089.jpg">

Note: Example:

CounterCreationDataCollection counterDataCollection = new CounterCreationDataCollection(); // Box1

// Add the counter. Box 1

CounterCreationData averageCount64 = new CounterCreationData(); averageCount64.CounterType = PerformanceCounterType.AverageCount64; averageCount64.CounterName = quot;AverageCounter64Samplequot;; counterDataCollection.Add(averageCount64);

// Add the base counter.

CounterCreationData averageCount64Base = new CounterCreationData(); averageCount64Base.CounterType = PerformanceCounterType.AverageBase; averageCount64Base.CounterName = quot;AverageCounter64SampleBasequot;; counterDataCollection.Add(averageCount64Base); // Box 2

// Create the category. Box 3 PerformanceCounterCategory.Create(quot;AverageCounter64SampleCategoryquot;, quot;Demonstrates usage of the AverageCounter64 performance counter type.quot;, PerformanceCounterCategoryType.SingleInstance, counterDataCollection);

</details> 



## <a name="59"></a> Question No: 59 – (Topic 1)
You use the Task.Run() method to launch a long-running data processing operation. The data processing operation often fails in times of heavy network congestion.

If the data processing operation fails, a second operation must clean up any results of the first operation.

You need to ensure that the second operation is invoked only if the data processing operation throws an unhandled exception.

What should you do?

```
A Create a TaskCompletionSourcelt;Tgt; object and call the TrySetException() method of the object.
B Create a task by calling the Task.ContinueWith() method.
C Examine the Task.Status property immediately after the call to the Task.Run() method.
D Create a task inside the existing Task.Run() method by using the AttachedToParent option.
```

<details>
  <summary>Answer</summary>
    B
</details> 

## <a name="60"></a> Question No: 60 – (Topic 1)
You use the Task.Run() method to launch a long-running data processing operation. The data processing operation often fails in times of heavy network congestion.

If the data processing operation fails, a second operation must clean up any results of the first operation.

You need to ensure that the second operation is invoked only if the data processing operation throws an unhandled exception.

What should you do?

```
A Create a task within the operation, and set the Task.StartOnError property to true.
B Create a TaskFactory object and call the ContinueWhenAll() method of the object.
C Create a task by calling the Task.ContinueWith() method.
D Use the TaskScheduler class to create a task and call the TryExecuteTask() method on the class.
```

<details>
  <summary>Answer</summary>
    C

    Explanation:

Task.ContinueWith – Creates a continuation that executes asynchronously when the target Task
</details> 


## <a name="61"></a> Question No: 61 – (Topic 1)
You are developing an application that uses structured exception handling. The application includes a class named ExceptionLogger.

The ExceptionLogger class implements a method named LogException by using the following code segment:

public static void LogException(Exception ex) You have the following requirements:

->Log all exceptions by using the LogException() method of the ExceptionLogger class.

->Rethrow the original exception, including the entire exception stack.

You need to meet the requirements. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_090.jpg">

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
A
 Explanation:

Once an exception is thrown, part of the information it carries is the stack trace. The stack trace is a list of the method call hierarchy that starts with the method that throws the exception and ends with the method that catches the exception. If an exception is re- thrown by specifying the exception in the throw statement, the stack trace is restarted at the current method and the list of method calls between the original method that threw the exception and the current method is lost. To keep the original stack trace information with the exception, use the throw statement without specifying the exception.

http://msdn.microsoft.com/en-us/library/ms182363(v=vs.110).aspx

</details>

## <a name="62"></a> Question No: 62 – (Topic 1)
You are developing an application by using C#. The application includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_091.jpg">

The DoWork() method must not throw any exceptions when converting the obj object to the IDataContainer interface or when accessing the Data property.

You need to meet the requirements. Which code segment should you insert at line 07?

```
A var dataContainer = (IDataContainer)obj;
B dynamic dataContainer = obj;
C var dataContainer = obj is IDataContainer;
D var dataContainer = obj as IDataContainer;
```

<details>
  <summary>Answer</summary>
D

As – The as operator is like a cast operation. However, if the conversion isn#39;t possible, as returns null instead of raising an exception.
</details>


http://msdn.microsoft.com/en-us/library/cscsdfbt(v=vs.110).aspx

## <a name="63"></a> Question No: 63 – (Topic 1)
You are developing an application that includes a class named UserTracker. The application includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_092.jpg">

You need to add a user to the UserTracker instance. What should you do?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_093.jpg">

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="64"></a> Question No: 64 DRAG DROP – (Topic 1)
You are developing an application that implements a set of custom exception types. You declare the custom exception types by using the following code segments:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_094.jpg">

The application includes a function named DoWork that throws .NET Framework exceptions and custom exceptions. The application contains only the following logging methods:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_095.jpg">

The application must meet the following requirements:

->When ContosoValidationException exceptions are caught, log the information by using the static void Log (ContosoValidationException ex) method.

->When ContosoDbException or other ContosoException exceptions are caught, log

the information by using the static void Log(ContosoException ex) method.

You need to meet the requirements.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_096.jpg">


<details>
  <summary>Answer</summary>

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_097.jpg">

</details>


## <a name="65"></a> Question No: 65 DRAG DROP – (Topic 1)
You are implementing a method that creates an instance of a class named User. The User class contains a public event named Renamed. The following code segment defines the Renamed event:

Public event EventHandlerlt;RenameEventArgsgt; Renamed;

You need to create an event handler for the Renamed event by using a lambda expression.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_098.jpg">


<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_099.jpg">
</details>


## <a name="66"></a> Question No: 66 – (Topic 1)
You are debugging an application that calculates loan interest. The application includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_100.jpg">

You need to ensure that the debugger breaks execution within the CalculateInterest() method when the loanAmount variable is less than or equal to zero in all builds of the application.

What should you do?

```
A Insert the following code segment at line 03: Trace.Assert(loanAmount > 0);
B Insert the following code segment at line 03: Debug.Assert(loanAmount > 0);
C Insert the following code segment at line 05: Debug.Write(loanAmount > 0);
D Insert the following code segment at line 05: Trace.Write(loanAmount > 0);
```

<details>
  <summary>Answer</summary>
A

By default, the Debug.Assert method works only in debug builds. Use the Trace.Assert method if you want to do assertions in release builds. For more information, see Assertions in Managed Code. http://msdn.microsoft.com/en-us/library/kssw4w7z.aspx
</details>


## <a name="67"></a> Question No: 67 – (Topic 1)
You are developing an application that includes a class named Order. The application will store a collection of Order objects.

The collection must meet the following requirements:

->Use strongly typed members.

->Process Order objects in first-in-first-out order.

->Store values for each Order object.

->Use zero-based indices.

You need to use a collection type that meets the requirements. Which collection type should you use?

```
A Queue<T>;
B SortedList
C LinkedList<T>;
D HashTable
C Array<T>;
```

<details>
  <summary>Answer</summary>
A

Queues are useful for storing messages in the order they were received for sequential processing. Objects stored in a Queuelt;Tgt; are inserted at one end and removed from the other.
http://msdn.microsoft.com/en-us/library/7977ey2c.aspx
</details>




## <a name="68"></a> Question No: 68 – (Topic 1)
You are developing an application. The application calls a method that returns an array of integers named employeeIds. You define an integer variable named employeeIdToRemove and assign a value to it. You declare an array named filteredEmployeeIds.

You have the following requirements:

->Remove duplicate integers from the employeeIds array.

->Sort the array in order from the highest value to the lowest value.

->Remove the integer value stored in the employeeIdToRemove variable from the employeeIds array.

You need to create a LINQ query to meet the requirements. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_101.jpg">

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
C
</details>

## <a name="69"></a> Question No: 69 – (Topic 1)
You have the following code (line numbers are included for reference only):

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_102.jpg">

You need to identify the missing line of code at line 15. Which line of code should you identify?

```
A using (fooSqlConn.BeginTransaction())
B while (fooSqlReader.Read())
C while (fooSqlReader.NextResult())
D while (fooSqlReader.GetBoolean(0))
```

<details>
  <summary>Answer</summary>
B
</details>

## <a name="70"></a> Question No: 70 DRAG DROP – (Topic 1)
You are developing a class named Temperature.

You need to ensure that collections of Temperature objects are sortable.

How should you complete the relevant code segment? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_103.jpg">

Answer:
<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_104.jpg">
</details>

## <a name="71"></a> Question No: 71 – (Topic 1)
You are creating a console application by using C#. You need to access the application assembly.

Which code segment should you use?

```
A Assembly.GetAssembly(this);
B this.GetType();
C Assembly.Load();
D Assembly.GetExecutingAssembly();
```
<details>
  <summary>Answer</summary>
D

Assembly.GetExecutingAssembly – Gets the assembly that contains the code that is currently executing.

http://msdn.microsoft.com/en- us/library/system.reflection.assembly.getexecutingassembly(v=vs.110).aspx

Assembly.GetAssembly – Gets the currently loaded assembly in which the specified class is defined.

http://msdn.microsoft.com/en-us/library/system.reflection.assembly.getassembly.aspx

</details>

## <a name="72"></a> Question No: 72 – (Topic 1)
You are developing an application by using C#. The application includes the following code segment. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_105.jpg">

The DoWork() method must throw an InvalidCastException exception if the obj object is not of type IDataContainer when accessing the Data property.

You need to meet the requirements. Which code segment should you insert at line 07?

```
A var dataContainer = (IDataContainer) obj;
B var dataContainer = obj as IDataContainer;
C var dataContainer = obj is IDataContainer;
D dynamic dataContainer = obj;
```
<details>
  <summary>Answer</summary>
A

http://msdn.microsoft.com/en-us/library/ms173105.aspx
</details>


## <a name="73"></a> Question No: 73 – (Topic 1)
You are developing a C# application that includes a class named Product. The following code segment defines the Product class:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_106.jpg">

You implement System.ComponentModel.DataAnnotations.IValidateableObject interface to provide a way to validate the Product object.

The Product object has the following requirements:

->The Id property must have a value greater than zero.

->The Name property must have a value other than empty or null.

You need to validate the Product object. Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_107.jpg">

```
A Option A
B Option B
C Option C
D Option D
```
<details>
  <summary>Answer</summary>
B
</details>

## <a name="74"></a> Question No: 74 DRAG DROP – (Topic 1)
You are developing an application that will include a method named GetData. The GetData() method will retrieve several lines of data from a web service by using a System.IO.StreamReader object.

You have the following requirements:

->The GetData() method must return a string value that contains the first line of the response from the web service.

->The application must remain responsive while the GetData() method runs.

You need to implement the GetData() method.

How should you complete the relevant code? (To answer, drag the appropriate objects to the correct locations in the answer area. Each object may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_108.jpg">


<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_109.jpg">
</details>


## <a name="75"></a> Question No: 75 – (Topic 1)
An application will upload data by using HTML form-based encoding. The application uses a method named SendMessage.

The SendMessage() method includes the following code. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_110.jpg">

The receiving URL accepts parameters as form-encoded values.

You need to send the values intA and intB as form-encoded values named a and b, respectively.

Which code segment should you insert at line 04?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_111.jpg">

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
D

WebClient.UploadValuesTaskAsync – Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object. These methods do not block the calling thread.

http://msdn.microsoft.com/en-us/library/system.net.webclient.uploadvaluestaskasync.aspx
</details>


## <a name="76"></a> Question No: 76 – (Topic 1)
You are developing an application by using C#. You provide a public key to the development team during development.

You need to specify that the assembly is not fully signed when it is built.

Which two assembly attributes should you include in the source code? (Each correct answer presents part of the solution. Choose two.)

```
A AssemblyKeyNameAttribute
B ObfuscateAssemblyAttribute
C AssemblyDelaySignAttribute
D AssemblyKeyFileAttribute
```
<details>
  <summary>Answer</summary>
C,D

http://msdn.microsoft.com/en-us/library/t07a3dye(v=vs.110).aspx
</details>

## <a name="77"></a> Question No: 77 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server. You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A RSA
B HMACSHA2S6
C Aes
D RNGCryptoServiceProvider
```

<details>
  <summary>Answer</summary>
B
</details>

## <a name="78"></a> Question No: 78 – (Topic 1)
You are developing an application by using C#.

You have the following requirements:

->Support 32-bit and 64-bit system configurations.

->Include pre-processor directives that are specific to the system configuration.

->Deploy an application version that includes both system configurations to testers.

->Ensure that stack traces include accurate line numbers.

You need to configure the project to avoid changing individual configuration settings every time you deploy the application to testers.

Which two actions should you perform? (Each correct answer presents part of the solution. Choose two.)

```
A Update the platform target and conditional compilation symbols for each application configuration.
B Create two application configurations based on the default Release configuration.
C Optimize the application through address rebasing in the 64-bit configuration.
D Create two application configurations based on the default Debug configuration.
```

<details>
  <summary>Answer</summary>
 B,D
</details>

## <a name="79"></a> Question No: 79 HOTSPOT – (Topic 1)
You are implementing a library method that accepts a character parameter and returns a string.

If the lookup succeeds, the method must return the corresponding string value. If the lookup fails, the method must return the value quot;invalid choice.quot;

You need to implement the lookup algorithm.

How should you complete the relevant code? (To answer, select the correct keyword in each drop-down list in the answer area.)


<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_112.jpg">

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_113.jpg">


<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_114.jpg">

Explanation:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_115.jpg">

http://msdn.microsoft.com/en-us/library/06tc147t(v=vs.110).aspx

</details>



## <a name="80"></a> Question No: 80 HOTSPOT – (Topic 1)
You are developing an application that includes a Windows Communication Foundation (WCF) service. The service includes a custom TraceSource object named ts and a method named DoWork. The application must meet the following requirements:

->Collect trace information when the DoWork() method executes.

->Group all traces for a single execution of the DoWork() method as an activity that can be viewed in the WCF Service Trace Viewer Tool.

You need to ensure that the application meets the requirements.

How should you complete the relevant code? (To answer, select the correct code segment from each drop-down list in the answer area.)



<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_116.jpg">

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_117.jpg">

<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_118.jpg">
</details>


## <a name="81"></a> Question No: 81 – (Topic 1)
You are developing an application that uses a .config file. The relevant portion of the .config file is shown as follows:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_119.jpg">

You need to ensure that diagnostic data for the application writes to the event tog by using the configuration specified in the .config file.

What should you include in the application code?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_120.jpg">

```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="82"></a> Question No: 82 – (Topic 1)
You are implementing a method named Calculate that performs conversions between value types and reference types. The following code segment implements the method. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_121.jpg">

You need to ensure that the application does not throw exceptions on invalid conversions. Which code segment should you insert at line 04?

```
A int balance = (int) (float)amountRef;
B int balance = (int)amountRef;
C int balance = amountRef;
D int balance = (int) (double) amountRef;
```
<details>
  <summary>Answer</summary>
A
</details>

## <a name="83"></a> Question No: 83 – (Topic 1)
You are developing an application that will transmit large amounts of data between a client computer and a server. You need to ensure the validity of the data by using a cryptographic hashing algorithm. Which algorithm should you use?

```
A RSA
B Aes
C HMACSHA256
D DES
```

<details>
  <summary>Answer</summary>
C
</details>

## <a name="84"></a> Question No: 84 – (Topic 1)
You are developing code for a class named Account. The Account class includes the following method:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_122.jpg">

You need to ensure that overflow exceptions are thrown when there is an error. Which type of block should you use?

```
A checked
B try
C using
D unchecked
```
<details>
  <summary>Answer</summary>
A
</details>

## <a name="85"></a> Question No: 85 DRAG DROP – (Topic 1)
You are developing an application by using C#. The application will output the text string quot;First Linequot; followed by the text string quot;Second Linequot;.

You need to ensure that an empty line separates the text strings.

Which four code segments should you use in sequence? (To answer, move the appropriate code segments to the answer area and arrange them in the correct order.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_123.jpg">


<details>
  <summary>Answer</summary>

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_124.jpg">


Explanation:

Box 1:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_125.jpg">

First we create the variable. Box 2:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_126.jpg">

We create the first text line. Box 3:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_127.jpg">

We add a blank line.

The StringBuilder.AppendLine method appends the default line terminator to the end of the current StringBuilder object.

Box 4:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_128.jpg">

Finally we add the second line.

</details>



## <a name="86"></a> Question No: 86 – (Topic 1)
You are developing an application that accepts the input of dates from the user.

Users enter the date in their local format. The date entered by the user is stored in a string variable named inputDate. The valid date value must be placed in a DateTime variable named validatedDate.

You need to validate the entered date and convert it to Coordinated Universal Time (UTC). The code must not cause an exception to be thrown.

Which code segment should you use?

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_129.jpg">


```
A Option A
B Option B
C Option C
D Option D
```

<details>
  <summary>Answer</summary>
A

AdjustToUniversal parses s and, if necessary, converts it to UTC.

Note: The DateTime.TryParse method converts the specified string representation of a date and time to its DateTime equivalent using the specified culture-specific format information and formatting style, and returns a value that indicates whether the conversion succeeded.

</details>

## <a name="87"></a> Question No: 87 DRAG DROP – (Topic 1)
You are developing an application that includes a class named Customer.

The application will output the Customer class as a structured XML document by using the following code segment:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_130.jpg">

You need to ensure that the Customer class will serialize to XML.

How should you complete the relevant code? (To answer, drag the appropriate code segments to the correct locations in the answer area. Each code segment may be used once, more than once, or not at all. You may need to drag the split bar between panes or scroll to view content.)


<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_131.jpg">

<details>
  <summary>Answer</summary>

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_132.jpg">

Explanation:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_133.jpg">

http://msdn.microsoft.com/en-us/library/3dkta8ya.aspx
</details>




## <a name="88"></a> Question No: 88 – (Topic 1)
You are creating a class named Employee. The class exposes a string property named EmployeeType. The following code segment defines the Employee class. (Line numbers are included for reference only.)

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_134.jpg">

The EmployeeType property value must meet the following requirements:

->The value must be accessed only by code within the Employee class or within a class derived from the Employee class.

->The value must be modified only by code within the Employee class.

You need to ensure that the implementation of the EmployeeType property meets the requirements.

Which two actions should you perform? (Each correct answer represents part of the complete solution. Choose two.)

```
A Replace line 03 with the following code segment: public string EmployeeType
B Replace line 06 with the following code segment: protected set;
C Replace line 05 with the following code segment: private get;
D Replace line 05 with the following code segment: protected get;
E Replace line 03 with the following code segment: protected string EmployeeType
F Replace line 06 with the following code segment: private set;
```
<details>
  <summary>Answer</summary>
E, F
</details>

## <a name="89"></a> uestion No: 89 – (Topic 1)
You are developing an application by using C#.

The application includes an object that performs a long running process.

You need to ensure that the garbage collector does not release the object#39;s resources until the process completes.

Which garbage collector method should you use?

```
A RemoveMemoryPressure()
B ReRegisterForFinalize()
C WaitForFullGCComplete()
D KeepAlive()
```

<details>
  <summary>Answer</summary>
D
</details>

## <a name="90"></a> Question No: 90 HOTSPOT – (Topic 1)
You have the following code:

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_135.jpg">

To answer, complete each statement according to the information presented in the code.

<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_136.jpg">


<details>
  <summary>Answer</summary>
<img src="http://www.pass-exams.com/pic/Microsoft-70-483.V13.01/Image_137.jpg">
</details>
