Здесь собраны вопросы и ответы на IT темы
----------------------------

### Вопросы [здесь](https://gitlab.com/VirG/Questions/blob/master/Questions.md)

### Вопросы с ответами [здесь](https://gitlab.com/VirG/Questions/blob/master/QuestionsAndAnswers.md)


### Экзамен 70-483 [здесь](https://gitlab.com/VirG/Questions/blob/master/Exam483/FullExam.md)