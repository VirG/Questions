public class Human
{
    public Point Position { get; set; } = new Point(0,0);

    public void CallDog_Bad(Dog dog)
    {
        dog.Legs.MoveOn(this.Position);//нарушает закон
    }

    public void CallDog_Good(Dog dog)
    {
        dog.MoveOn(this.Position);//не нарушает закон
    }
}